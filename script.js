
 var dropArea=document.getElementById('droparea');
dropArea.addEventListener("dragenter", dragenter, false);
dropArea.addEventListener("dragover", dragover, false);
dropArea.addEventListener("drop",drop ,  false);
function dragenter(e) {
  event.stopPropagation();
  event.preventDefault();
}

function dragover(e) {
  event.stopPropagation();
  event.preventDefault();
} 
function drop(e) {
  event.stopPropagation();
  event.preventDefault();
}

;['dragenter', 'dragover'].forEach(eventName => {
  dropArea.addEventListener(eventName, highlight, false)
})
;['dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, unhighlight, false)
})
 dropArea.addEventListener('drop',handleDrop);
function highlight(event) {
  dropArea.classList.add('highlight')
}
function unhighlight(event) {
  dropArea.classList.remove('highlight')
}
function handleDrop(event) {
  let dt = event.dataTransfer;
  let files = dt.files;
  handleFiles(files)
  
}
function handleFiles(files) {
  for (var i = 0; i < files.length; i++) {
    var file = files[i];
    if (!file.type.startsWith('image/')){ 
	continue
	}
    var img = document.createElement("img");
    img.classList.add("draggable");
    img.file = file;
    dropArea.appendChild(img); 
    var reader = new FileReader();
    reader.onload = (function(aImg) { 
		return function(e) { 
		var j=dropArea.appendChild(img); 
		
		 var bar=document.getElementById('myBar');
		 bar.style.width=(event.loaded /event.total)*13 +'%'
			aImg.src = e.target.result;
			
		 }
		
				
})(img);
	reader.onprogress=function(event){
		 if (event.lengthComputable) {
	//console.log(reader.result);
		 }
	
	}
  reader.readAsDataURL(file);
		
  }
}
  //function upload()
 // }
 //}
   
 //var dragimg=document.getElementsByClassName('draggable');
 //console.log(dragimg)
var DragManager = new function() {


  var dragObject = {};

  var self = this;

  function onMouseDown(e) {

    if (e.which != 1) return;

    var elem = e.target.closest('.draggable');
    if (!elem) return;

    dragObject.elem = elem;

    
    dragObject.downX = e.pageX;
    dragObject.downY = e.pageY;

    return false;
  }

  function onMouseMove(e) {
    if (!dragObject.elem) return; 

    if (!dragObject.avatar) { 
      var moveX = e.pageX - dragObject.downX;
      var moveY = e.pageY - dragObject.downY;

      
      if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
        return;
      }

      
      dragObject.avatar = createAvatar(e); 
      if (!dragObject.avatar) { 
        dragObject = {};
        return;
      }

 
      var coords = getCoords(dragObject.avatar);
      dragObject.shiftX = dragObject.downX - coords.left;
      dragObject.shiftY = dragObject.downY - coords.top;

      startDrag(e);
    }

   
    dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
    dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';

    return false;
  }

  function onMouseUp(e) {
    if (dragObject.avatar) { 
      finishDrag(e);
	  
    }

  
    dragObject = {};
  }

  function finishDrag(e) {
    var dropElem = findDroppable(e);

    if (!dropElem) {
      self.onDragCancel(dragObject);
    } else {
      self.onDragEnd(dragObject, dropElem);
	  
    }
  }

  function createAvatar(e) {

    
    var avatar = dragObject.elem;
    var old = {
      parent: avatar.parentNode,
      nextSibling: avatar.nextSibling,
      position: avatar.position || '',
      left: avatar.left || '',
      top: avatar.top || '',
      zIndex: avatar.zIndex || ''
    };

    
    avatar.rollback = function() {
      old.parent.insertBefore(avatar, old.nextSibling);
      avatar.style.position = old.position;
      avatar.style.left = old.left;
      avatar.style.top = old.top;
      avatar.style.zIndex = old.zIndex
    };

    return avatar;
  }

  function startDrag(e) {
    var avatar = dragObject.avatar;

  
    document.body.appendChild(avatar);
    avatar.style.zIndex = 9999;
    avatar.style.position = 'absolute';
  }

  function findDroppable(event) {
	  	console.log(event);
    dragObject.avatar.hidden = true;
   
    var elem = document.elementFromPoint(event.clientX, event.clientY);

   
    dragObject.avatar.hidden = false;

    if (elem == null) {
      
      return null;
    }

    return elem.closest('.droppable');
	console.log(event)
  }

  document.onmousemove = onMouseMove;
  document.onmouseup = onMouseUp;
  document.onmousedown = onMouseDown;

  this.onDragEnd = function(dragObject, dropElem) {};
  this.onDragCancel = function(dragObject) {};
};


function getCoords(elem) { 
  var box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset
  };

}
function UploadFiles(img,file){
//	reader=new FileReader();
	
	
	

  reader.onload=function(event){
		bar.style.width = ('100px');
		console.log(reader.result);
		   reader.readAsText(file);
	}
}
